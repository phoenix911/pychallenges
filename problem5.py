import pickle
from os import path


path = path.realpath("files/banner.p")
file = open(path, 'rb')
data = pickle.load(file)


print '\n'.join([''.join([p[0] * p[1] for p in row]) for row in data])
