import re
from os import path

path = path.realpath("files/problem3.txt")

f = open(path, 'r')
print "".join(re.findall("[^A-Z]+[A-Z]{3}([a-z])[A-Z]{3}[^A-Z]+", f.read()))
